const browserSync	= require('browser-sync').create();
const concat 	= require('gulp-concat');
const imagemin 	= require('gulp-imagemin');
const gulp 	= require('gulp');
const nib 	= require('nib');
const pug 	= require('gulp-pug');
const stylus 	= require('gulp-stylus');
const uglify 	= require('gulp-uglify');



gulp.task('serverAptitud', function(){
	browserSync.init({
		server: '../base-maquetacion-pug-stylus/public',
		port: 1917
	});
	gulp.watch("assets/img/*.", gulp.series("executeImage"));
	gulp.watch("assets/js/*.js", gulp.series("executeJs"));
	gulp.watch("assets/templates/*.pug", gulp.series("executePug"));
	gulp.watch("assets/stylus/*.styl", gulp.series("executeStylus"));
});

gulp.task('executeImage',function(){
	 return gulp.src('assets/img/*.{png,jpg,jpeg,gif,svg}')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img/'))
        .pipe(browserSync.stream());
});


gulp.task('executeJs',function(){
	 return gulp.src('assets/js/*.js')
        .pipe(concat('app.js'))
				.pipe(uglify())
        .pipe(gulp.dest('public/js/'))
        .pipe(browserSync.stream());
});

gulp.task('executePug',function(){
	return gulp.src('assets/templates/*.pug')
		.pipe(pug())
		.pipe(gulp.dest('public'))
		.pipe(browserSync.stream());
});

gulp.task('executeStylus',function(){
	return gulp.src('assets/stylus/*.styl')
        .pipe(stylus({ use: nib(), compress: true }))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.stream());
});
