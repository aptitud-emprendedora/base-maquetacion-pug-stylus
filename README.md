# Base maquetación para usar Pug y Stylus

--------------------------------------------------------
# Programas a Instalar

* Atom(Opcional)
* Hyper.is(Opcional)
* Nodejs(Obligatorio)

# Primer paso de configuración
- Clonar el proyecto en el lugar que desees de tu computador
- Abrir un terminal para instalar GulpJS:
* npm install -g gulp(En Windows)
* npm install gulp(En Linux)

# Segundo paso de configuración
Ubícate en la carpeta que haz clonado el proyecto, abres un terminal y lo haz lo siguiente:

## Instalar dependencias
npm install

## Abrir atom (Opcional)
atom .

## Desplegar entorno de Desarrollo
gulp serverAptitud

--------------------------------------------------------

## Las herramientas a usar son las siguientes:

Servidor local:
+ browser-sync

Sistema de control versiones
+ Git

Preprocesadores:
+ PUG(HTML)
+ Stylus(CSS)

Paquetes:
+ browser-sync
+ gulp-concat
+ gulp-imagemin
+ gulp-pug
+ gulp-stylus
+ gulp-uglify
+ nib
